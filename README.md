## Carvana Image Masking Challenge

### Basic Info
This repository contains the code necessary to reproduce/generate a top-5% segmentation pipeline for the Carvana Image Masking Challenge. 
The data required is available through [Kaggle](https://www.kaggle.com/c/carvana-image-masking-challenge).

---
### Note on running the scripts:
The code in this repository is build around tensorflow 1.3 and Keras 2.1. For usage of recent releases, the code needs to be updated respectively.
All other packages used are available within standard Anaconda.

---
### This repository contains the following files:

1. `network_training.py`: Network training script with Network Scaffold taken from `network_library.py`.
2. `network_library.py`: Contains a [UNet-Scaffold](https://arxiv.org/abs/1505.04597) in addition to a Scaffold for the [Hundred-Layer Tiramisu](https://arxiv.org/abs/1611.09326), as well as certain auxiliary functions.
3. `helper_functions.py`: Holds the data generator to (optionally) pass to Keras. Also contains various data handling functions.
4. `create_bordermaps.py`: Script to generate boundary weight masks around car borders to place higher emphasis on correct assignment of pixels close to the border. 
5. `validate_test.py`: Use the pretrained network to generate run-length encoded output segmentation. 

---
### The actual pipeline has the following structure:   
`create_bordermaps.py` to generate weightmaps to place more weights on accurate boundary segmentation `>>`   
`network_training.py` to train the network of choice with boundary masks `>>`   
`validate_test.py` to run the trained network on the test dataset.

---
### Example usage
Each of the elementary pipeline files can be exemplary run with the following input arguments:

_Create Boundary Mask_: 

```
python create_bordermaps.py --path_to_training_masks <path-to-ground-truth-target-mask> --save_path <location-of-choice-to-save-computed-masks>
```

_Train Network_: 

```
python network_training.py --path_to_input_images <path-to-training-images> --path_to_ground_truth_masks <path-to-target-segmentation-masks> --path_to_boundary_masks <path-to-precomputed-boundary-masks>
```

_Generate Test Results_: 
Please edit the paths to the test data and the choice of network weights withing the file, then run ```python validate_test.py```.